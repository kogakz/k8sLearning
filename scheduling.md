# shedulerの勉強

## 参考サイト

公式サイト
https://kubernetes.io/docs/concepts/scheduling-eviction/scheduling-framework/

自作して学ぶKubernetes Scheduler
https://engineering.mercari.com/blog/entry/20211220-create-your-kube-scheduler/


スケジューリングフレームワークの詳細サイト
https://github.com/kubernetes/enhancements/blob/master/keps/sig-scheduling/624-scheduling-framework/README.md


## 公式サイト確認結果

### 概要

Kubernetes v1.19で`stable`になっている。

- スケジューリングの処理フロー
![スケジューリングの流れ](images/scheduling.png)

- PodのQueでの流れ
![PodのQue間の流れ](images/que.png)

  - 補足1
    - ActiveQueに入るにはすべての`PreEnqueプラグイン`が`Success`を返す必要がある。
    - いずれかが`Success`を返さなかった場合、`internal unschedulable Pods list`に保持される。なお、この時のPodのステータスは**Unschedulableですらない**。
    - `internal unschedulable Pods list`に入ったPodの扱い
      - `PreEnqueue`、`PreFilter`、`Filter`、`Reserve`、`Permit`を実装するプラグインが`EnqueueExtension`インタフェースに基づいてPodをスケジューリングを再試行する。
      - 考え方（想定）
        - 各pluginは、それぞれの観点にのみスケジューリングしてよいかどうかをチェックすればOKという、役割分担をする思想
  - ActiveQueの補足
    - 優先度の高い順になっている。
    - 優先度は、`QueueSort`拡張ポイントでカスタマイズできる。
    - 新しいPodが作成される場合、`.spec.nodeName`が空`empty`の状態で登録される。
  - UnshcedulableQになったQueが再度AcvieQue,BackOffQに移動する契機
    - `Moving request`が呼び出されること
    - クラスター イベントが非同期に`Moving request`を発生させる。
    - `Moving request`が発生するクラスタイベントは、`pods`、`nodes`、`services`、`PVs`、`PVCs`、`storage classes`、`CSI nodes`のchangeがあります。

### 各フェーズの説明

#### フェーズの概要

| No |サイクル|フェーズ|概要
|:-:|:-|:-|:-
|1| - | PreEnque | ActiveQueに入れるかどうかのチェックをする
|2| - | QueueSort | キュー内のポッドをソートする。※1つしか登録できない
|3| Scheduling | PreFilter | 対象のPodの情報をもとに先に進んでいいかをチェックする。
|4| Scheduling | Filter| PreFileterを通ったPodの実行先として、実行できないノードを除外する。
|5| Scheduling | PostFilter | PreFileterで実行可能なノードが見つからなかったPodの起動先を再評価する。依存関係があるPodとかに使う？
|6| Scheduling | PreScore | Scoreが参照する元ネタを作る？
|7| Scheduling | Score | Filterで`Scheculable`として残ったノードに対して起動先を判定するため、全`scoring plugin`を呼び出してスコアを取得する。
|8| Scheduling | NormalizeScore | Scoreで返却された値が、それぞれのノードに定められたスコアの最大値よりも大きい場合に、その値を是正する。各プラグインで是正方法も実装する。
|9| Scheduling | Reserve | ノード上のリソースを予約するフェーズ。Reserve インターフェースを実装するプラグインはReserveメソッドで実際のリソースを予約する。失敗したときのお掃除用の処理はUnreserveメソッドで実装する。
|10| Scheduling | Permit | 候補ノードへのバインドを防止または遅延させるためのフェーズ。**wait**を返せば待機させられる。
|11| Binding | PreBind | Bindフェーズ前に必要な事前作業を実施する。（例：ネットワークボリュームのマウントなど）
|12| Binding | Bind | ノードにBindするための処理を実施する。Podのステータスとして、対象のノードを書くことでよい？
|13| Binding | PostBind | Pod が正常にバインドされた後に呼び出される。関連するリソースをクリーンアップするために使用できます。とあるが、実際に何やる？

#### PreEnque

- すべての`PreEnqueue`プラグインが`Success`と返したら、`ActiveQue`に入る

#### Scheduling Cycle

#### Binding Cycle

---

#### Schedulerプラグイン仕様案

- 要件
  - 実際の起動処理に入る前までは進めたい。ステータスはPedningになる想定。
  - 起動許可をするのは対象のノードのCPU使用率が一定値以下である場合のみ
  - Podを起動する処理でスパイクするCPU使用率を足しても、CPU使用率が一定値以下になること
  - 起動時のCPU使用率の見込みはPodのアノテーションなどでに設定する前提
  - 起動中かどうかは、PodのPhaseで判断するか、コンテナのStatusまで見るかは要整理。
  - 同一ノード上で実行される起動処理は、上記を考慮したうえで可能な限り並列で実行すること
    - 難しい場合は１つずつでもOK
    - 制御するのは特定種類のPodだけにする。（業務用のPod以外は素直に上がってほしい）
    - ラベルとかで識別できる

#### Podの起動状態について

7/26時点で整理中。

- Podの状態はPhase
- コンテナの状態はStatus

言葉が違うのに初めて気が付いた。

https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/

PodのPhaseの図が分かりやすい

https://bobcares.com/blog/kubernetes-pod-states/

![わかりやすいPod Phaseの図](image.png)


##### PodのPhase

| 価値 | 形容 |
| ---- | ---- |
| Pending | Pod は Kubernetes クラスターによって受け入れられましたが、1 つ以上のコンテナーが設定されておらず、実行する準備ができていません。これには、Podがスケジュールされるのを待つのに費やす時間や、ネットワーク経由でコンテナイメージをダウンロードするのに費やした時間が含まれます。 |
| Running | Podはノードにバインドされ、すべてのコンテナが作成されています。少なくとも 1 つのコンテナがまだ実行されているか、起動または再起動中です。 |
| Succeeded | Pod内のすべてのコンテナは正常に終了し、再起動されません。 |
| Failed | Pod内のすべてのコンテナが終了し、少なくとも1つのコンテナが失敗して終了しました。つまり、コンテナがゼロ以外のステータスで終了したか、システムによって終了され、自動再起動が設定されていません。 |
| Unknown | 何らかの理由でPodの状態を取得できませんでした。このフェーズは通常、Podが実行されているはずのノードとの通信中にエラーが発生したときに発生します。 |

###### Kubernetesのコンテナのライフサイクル
```plantuml
@startuml
title Kubernetesのコンテナのライフサイクル

state "待機中 (Waiting)" as waiting
state "実行中 (Running)" as running
state "終了 (Terminated)" as terminated

[*] --> waiting
waiting --> running : スタートアップ操作が完了\npostStartフックが実行される
running --> terminated : 実行が完了または失敗\npreStopフックが実行される
terminated --> [*]

note right of waiting
  コンテナが「実行中 (Running)」または「終了 (Terminated)」の状態でない場合、それは「待機中 (Waiting)」の状態です。
  これは、コンテナが起動を完了するために必要な操作をまだ実行していることを意味します。
  たとえば、コンテナイメージをレジストリからプルするか、Secretデータを適用するなどです。
  待機中のコンテナがあるPodを問い合わせると、「Reason」フィールドも表示され、コンテナがその状態にある理由を要約します。
end note

note right of running
  「実行中 (Running)」のステータスは、コンテナが問題なく実行していることを示します。
  postStartフックが設定されていた場合、それはすでに実行されて**終了**しています。
  実行中のコンテナがあるPodを問い合わせると、コンテナが実行中の状態に入ったときの情報も表示されます。
end note

note right of terminated
  「終了 (Terminated)」の状態のコンテナは、実行を開始し、その後完了したか何らかの理由で失敗しました。
  終了したコンテナがあるPodを問い合わせると、理由、終了コード、およびそのコンテナの実行期間の開始と終了の時間が表示されます。
  コンテナにpreStopフックが設定されていた場合、このフックは**コンテナが終了状態に入る前に実行**されます。
end note

@enduml

```

そのほかに調べたこと
https://github.com/kubernetes/enhancements/blob/master/keps/sig-scheduling/3521-pod-scheduling-readiness/README.md

- `.spec.schedulingGates`
  - Enqueに入れる、入れないのための拡張用スペック（Enque Pluginのデフォルト実装あり）
    - Podが依存するリソースが長時間使えない状態のときに、スケジューリングチェック対象になるとNG→再チェック→NG→再チェックみたいな感じで、スケジューリング処理に滞留してしまう。
    - その影響で、他のPodのスケジューリングが待たされてしまうことが起きる。
    - そのため、スケジューリングのチェック対象にしても無駄になってしまうPodをEnqueしないようにする。
  - しないこと
    - Enque済みのものは、このステータスではいじらない。
    - Enque済みのPodは{type:PodScheduled, reason:Unschedulable}で管理することを崩さないようにする。
  - 調査結果
    - 今回の用途では使わない。
    - バインディングまでは進めて待たせたいというのがやりたいことなので。




